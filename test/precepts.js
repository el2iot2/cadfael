//var when = require('when');

module.exports = function(cadfael) {
  return cadfael
    .observantFor(module, precepts)
    .withChaiShould();
};

function precepts(O) {
  return {
    //Run basic tests
    err_no_export: function() { return O.observe('err_no_export.js').should.be.rejected.with(Error); },
    err_exports_object: function() { return O.observe('err_exports_object.js').should.be.rejected.with(Error); },
    success: function() { return O.observe('success.js'); },
    all:['err_no_export', 'err_exports_object', 'success']
  };
}