var fn = require('when/function');

module.exports = function(cadfael) {
  return cadfael
    .observantFor(module, precepts);
};

function precepts(O) {
  return {
    //Run basic tests
    a: function() { return fn.call(function(){return 40;}) },
    b: function() { return fn.call(function(){return 2;})  },
    add: {
      after: ['a', 'b'],
      then: function(a, b) {
        this.info('a=%j, b=%j',a, b);
        return a+b;
      }
    },
    all: { 
      after: 'add',
      then: function(sum) {
        this.info('sum=%j',sum);
        return sum === 42;
      }
    }
  };
}