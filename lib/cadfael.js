/*
 * cadfael
 * https://github.com/automatonic/cadfael
 *
 * Copyright (c) 2013 Elliott B. Edwards
 * Licensed under the MIT license.
 */

var _ = require('underscore');
var when = require('when');
var fn = require('when/function');
var C = exports;
var path = require('path');
var L = require('logule').init(module);
var Observant = require('./observant.js');
var LoggedError = require('./loggederror.js');
var FormattedError = require('./formattederror.js');

//Gets the current version of the library
C.version = function() {
  return require('../package.json').version;
};

//Creates a new Observant for the specified module and precepts
C.observantFor = function(_module, preceptsFunc) {
  if (!_module) {
    throw new FormattedError(L, 'expected module. got %j', preceptsFunc);
  }
  if (!_.isFunction(preceptsFunc)) {
    throw new FormattedError(L, 'expected preceptsFunc. got %j', preceptsFunc);
  }
  return new Observant(C, _module, preceptsFunc);
};


function isAbsolutePath(p) {
  return path.resolve(p) === p.replace(/[\/\\]+$/, '');
}

//Handle the contents of precept file
function observe(preceptFile, precept, moduleContext) {
  L.debug('reading %s', preceptFile);
  if (!isAbsolutePath(preceptFile) && moduleContext) {
    L.debug('detected relative path %j', preceptFile);
    preceptFile = path.join(
      path.dirname(moduleContext.filename),
      preceptFile);
    L.debug('coerced to %j', preceptFile);
  }
  
  var _module = null;
  try {
    _module = require(preceptFile);
  }
  catch(err) {
    throw new FormattedError('could not observe %j because: %j', preceptFile, err.message);
  }
  
  if (!_.isFunction(_module)) {
    throw new FormattedError('%j did not define module.exports to be a function', preceptFile);
  }
  
  var observant = _module(C);
  
  if (!_.isObject(observant)) {
    throw new FormattedError('expected module.exports to return an observant instance, but it returned %j', observant);
  }
  return observant.observePrecept(precept, moduleContext);
}

C.observe = function(preceptFile, precept, moduleContext) {
  precept = precept || 'all';
  //Handle a single precept file
  if (_.isString(preceptFile) && _.isString(precept)) {
    return fn.call(observe, preceptFile, precept, moduleContext);    
  }
  else {
    return when.reject(new LoggedError(L, '"observe" expected file and precept would be strings'));   
  }
};

C.rootObserve = function(preceptFile, precept) {
  L.debug('observing %j in %j', precept, preceptFile);
  module.preceptFile = preceptFile;
  return C
    .observe(preceptFile, precept, null)
    .otherwise(function(error) {
        L.error(error.stack);      
    });
};


