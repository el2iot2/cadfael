/*
 * cadfael
 * https://github.com/automatonic/cadfael
 *
 * Copyright (c) 2013 Elliott B. Edwards
 * Licensed under the MIT license.
 */

//Observant (noun) - one who observes forms and rules

var _ = require('underscore');
var when = require('when');
var apply = require('when/apply');
var O = exports;
var LoggedError = require('./loggederror.js');
var FormattedError = require('./formattederror.js');
var logule = require('logule');
var globFunc = require('glob');
var path = require('path');

function Observant(lib, _module, preceptsFunc) {
    this.lib = lib;
    this.precepts = preceptsFunc(this);
    this.promises = {};
    this.preceptFuncs = {};
    var relativePath;
    if (_module.parent.preceptFile) {
      relativePath = path.relative(path.dirname(_module.parent.preceptFile), _module.filename);
    }
    this.log = logule.init(_module, relativePath);
    this._module = _module;
}

function expectPromise(hoc, obj) {
  if (!when.isPromise(obj)) {
    throw new LoggedError(hoc.log, 'expected valid promise. got %j',obj);
  }
  return obj;
}

function promiseIsFulfilled(promise) {
  return expectPromise(this,promise).isFulfilled();
}

function promiseIsNotFulfilled(promise) {
  return !expectPromise(this,promise).isFulfilled();
}

function valueOfPromise(promise) {
  return promise.valueOf();
}

function buildRejectWith(error) {
  return function rejectWith() { throw error; };
}

function buildFulfillWith(value) {
  return function fulfillWith() { return value; };
}

function buildFulfiller(obj, log)
{
  if (_.isFunction(obj)) {
    return _.bind(obj, log);
  }
  else if (obj instanceof Error) {
    return buildRejectWith(obj);
  }
  else {
    return buildFulfillWith(obj);
  }
}

function functionOrNull(obj)
{
  if (_.isFunction(obj)) {
    return obj;
  }
  return null;
}

function expectPreceptNames(log, obj)
{
  //support a single "after" string
  if (_.isString(obj)) {
    obj = [obj];
  }
  
  //support an array of "after" precepts
  if (_.isArray(obj)) {
    if (!_.all(
      obj, 
      function(item) { 
        return _.isString(item); })) {
      throw new LoggedError(log, 'expected an array of strings, but got: %j ', obj);
    }
    return obj;
  }
  return null;
}

function notAfter(hoc, log, names, onFulfilled, onRejected, onProgress) {
  log.info('starting');
  return when.resolve(onFulfilled());
}

function defaultOnRejected(error) {
  return when.reject(error);
}

function compileThen(hoc, log, afterFunc, afterNames, onFulfilled, onRejected, onProgress) {
  onRejected = onRejected || defaultOnRejected;
  return function compiledThen() {
    return afterFunc(hoc, log, afterNames, onFulfilled, onRejected, onProgress);
  };
}

function compilePrecept(hoc, name, precept) {
  var afterNames;
  var onFulfilled;
  var onRejected;
  var onProgress;
  var preceptLog = hoc.log.sub(name);
  var log = preceptLog.sub('compile');
  
  if (_.isFunction(precept)) {
    log.debug('"then" directive');
    onFulfilled = buildFulfiller(precept, preceptLog);
  }
  else
  {
    afterNames = expectPreceptNames(log, precept);
    if (afterNames) {
      log.debug('"after" directive: %j', afterNames);  
    }
    //also consider a hash
    else if (precept) {
        
      if (precept.after) {
        afterNames = expectPreceptNames(log, precept.after);
        if (afterNames) {
          log.debug('".after" directive: %j', afterNames);
        }
      }
      
      if (precept.onFulfilled) {
        log.debug('".onFulfilled" directive');
        onFulfilled = buildFulfiller(precept.onFulfilled, preceptLog);
      }
      else if (precept.then) {
        log.debug('".then" alias for onFulfilled');
        onFulfilled = buildFulfiller(precept.then, preceptLog);
      }
      
      if (precept.onRejected) {
        log.debug('".onRejected" directive');
        onRejected = functionOrNull(precept.onRejected);
      }
      
      if (precept.onProgress) {
        log.debug('".onProgress" directive');
        onProgress = functionOrNull(precept.onProgress);
      }
    }
  }

  return compileThen(
    hoc,
    preceptLog, 
    afterNames ? after : notAfter,
    afterNames,
    onFulfilled, 
    onRejected, 
    onProgress);
}

function observePrecept(hoc, name) {
  var preceptFunc = hoc.preceptFuncs[name];
  if (!preceptFunc) {
    hoc.log.debug('could not find preceptFunc named %j', name);
    var precept = hoc.precepts[name];
    if (!precept) {
      throw new LoggedError(hoc.log, 'could not find precept %j', name);
    }
    preceptFunc = compilePrecept(hoc, name, precept);
    hoc.preceptFuncs[name] = preceptFunc;
    if (!preceptFunc) {
      throw new LoggedError(hoc.log, 'preceptFunc compile failed for %j', name);
    }
  }
  return preceptFunc();
}

Observant.prototype.observePrecept = function(name) {
  return observePrecept(this, name); 
};

//Wait until the named precepts finish, then do somethings
function after(hoc, log, names, onFulfilled, onRejected, onProgress) {
  log.info('starting after %j', names);
  if (!names || !_.isArray(names) || !_.any(names)) {
    throw new LoggedError(log, 'expected an array of names, but got %j', names);
  }
  
  var namedPromises = 
  _.map(names, function(name) {
    var promise = hoc.promises[name];
    if (promise) {
      log.debug('%j using cached %j', names, name);
      return promise;
    }
    
    promise = observePrecept(hoc, name);
    hoc.promises[name] = promise;
    
    if (!when.isPromise(promise)) {
      throw new LoggedError(log, 'precept %j did not produce a valid promise: %j', name, promise);
    }
      
    return promise;
  });
  
  onFulfilled = function() {
    log.info('fulfilled');
    apply(onFulfilled);
  };
  
  onRejected = function(error) {
    log.info('rejected');
    throw error;
  };
  
  return when.all(namedPromises)
    .then(onFulfilled, onRejected, onProgress);
}

Observant.prototype.observe = function(preceptFile, precept) {
    return this.lib.observe(preceptFile, precept, this._module);
};

function globPattern(pattern, options) {
  var deferred = when.defer();
  globFunc(pattern, options, function (er, paths) 
  {    
      if (er) {
          deferred.reject(new Error(er));
      } else {
          deferred.resolve(paths);
      }
  });
  return deferred.promise;  
}

function globPatterns(patterns, options) {
  if (!patterns) {
    throw new LoggedError('expected glob pattern(s)');
  }
  if (!_.isArray(patterns)) {
    return globPattern(patterns, options);
  }
  var promises = _.map(patterns, function(pattern) {
    return globPattern(pattern, options);
  });
  
  return when
    .all(promises)
    .then(
      function (results) {
        var paths = [];
        results.forEach(function (promise) {
          paths.push(promise.valueOf());
        });
        return _.uniq(_.flatten(paths)); 
      },
      function(error) {
        throw error;
      }
    );
}
Observant.prototype.glob = globPatterns;

function globEach(patterns, pathPromiseFunc, options) {
  return globPatterns(patterns, options)
    .then(function(paths) {
      return when
        .all(_.map(paths, pathPromiseFunc));
    });
}
Observant.prototype.globEach = globEach;

Observant.prototype.withJshint = function(options, globals, globOptions, mixinName) {
  var jshint = this._module.require('jshint-as-promised').jshint;
  var hoc = this;
  //default
  options = options || {
    curly : true,
    eqeqeq : true,
    immed : true,
    latedef : true,
    newcap : true,
    noarg : true,
    sub : true,
    undef : true,
    boss : true,
    eqnull : true,
    node : true
  };
  
  globals = globals || {
      exports : true              
  };
  
  return this.withMixin(mixinName||'jshint', function(patterns) {
    function pathToJshintPromise(filePath) {
      return jshint(filePath, options, globals);
    }
    hoc.log.debug('globbing %j for jshint', patterns);
    return globEach(patterns, pathToJshintPromise, globOptions);
  });
};

Observant.prototype.withMixin = function(mixinName, mixinFunc) {
  this.log.debug('mixed in %j', mixinName);
  if (this[mixinName]) {
    throw new FormattedError('cannot mixin %j. name already exists.');
  }
  this[mixinName] = mixinFunc;
  return this;
};

Observant.prototype.withChaiShould = function() {
  var chai = require("chai");
  chai.should();
  var chaiAsPromised = require("chai-as-promised");
  chai.use(chaiAsPromised);
  return this;
};

module.exports = Observant;