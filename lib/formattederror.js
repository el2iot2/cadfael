var util = require('util');
var format = util.format;

//A custom error that takes formats
function FormattedError() {
  this.message = format.apply(null, arguments);
  if (Error.captureStackTrace) {
      Error.captureStackTrace(this, this.constructor);
  }
}

util.inherits(FormattedError, Error);

module.exports = FormattedError;