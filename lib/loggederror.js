var util = require('util');
var format = util.format;
var _ = require('underscore');

//A custom error that preserves
//the stack trace and the
//jshint error data
function LoggedError() {
  var log = _.first(arguments);
  log.error(this.message = format.apply(null, _.rest(arguments)));
  if (Error.captureStackTrace) {
      Error.captureStackTrace(this, this.constructor);
  }
}

util.inherits(LoggedError, Error);

module.exports = LoggedError;