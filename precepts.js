module.exports = function(cadfael) {
  return cadfael.observantFor(module, precepts);
};

function precepts(O) {
  return {
    //Run basic tests
    test: function() { return O.observe('./test/precepts.js'); },
    all:['test']
  };
}