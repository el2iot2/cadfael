# cadfael

A [monastic](https://github.com/automatonic/cadfael/wiki/Cadfael) javascript make/build/test tool that observes your [asynchronous promises](https://github.com/promises-aplus/promises-spec)

## Getting Started

> A _precept_ is "a general rule intended to regulate behavior or thought"

Cadfael automates and verifies the execution of interdependent [precepts](https://github.com/automatonic/cadfael/wiki/precepts.js)
 that you define for your project:

 1. Install the global CLI [script](https://github.com/automatonic/cadfael-cli): `npm install -g cadfael-cli`
 2. Write a [precept file](https://github.com/automatonic/cadfael/wiki/precepts.js)
 3. Install cadfael package in your project directory: `npm install cadfael --save-dev` (or globally, if you prefer)
 3. _Observe_ all of your precepts: `monk observe` (or just some of them: `monk observe tests`)

##API

If you don't need/want the global CLI, you can also use the API directly:

```javascript
var cadfael = require('cadfael');
cadfael.observeRoot('./precepts.js', 'tests'); //returns a promise
```
##Why another build tool?

`[m|r|j]ake` has systems of dependencies building only what is needed/changes. `grunt` lets you do something similar in node/javascript. Ever since I stumbled onto
the spec for promises, however, I have been itching to synthesize promises and build scripts. A general purpose, promise-based, dependency-aware build tool can singly and elegantly fulfill
the space currently held by an amagamate of other tools. Futhermore, this tool intends asynchronous support as first-class feature.

At least, that is the plan. Feedback and constructive criticism is welcome.

## Contributing
In lieu of a formal styleguide, take care to maintain the existing coding style. Add unit tests for any new or changed functionality. Lint and test your code using cadfael and the provided `precepts.js` file.

## Release History

 * 0.1.0 - Basic tooling working - Alpha functionality

## License
Copyright (c) 2013 Elliott B. Edwards  
Licensed under the MIT license.
